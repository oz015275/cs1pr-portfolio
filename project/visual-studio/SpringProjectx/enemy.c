#include "common.h"

static void tick(void);
static void touch(Entity* other);


void initEnemy(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->tick = tick;
	e->touch = touch;

	e->texture = loadTexture("gfx/enemy.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);

}


static void tick(void)
{
	//start moving
	if (self->dx == 0)
	{
		self->dx = ENEMY_MOVE_SPEED;
	}

	// get enemy position
	int rmx, lmx, my;

	rmx = (self->x + self->w) / TILE_SIZE;
	lmx = self->x / TILE_SIZE;

	my = (self->y / TILE_SIZE);

	// reverse directions if hit something solid
	if (stage.map[rmx][my] != 0)
	{
		self->dx = -ENEMY_MOVE_SPEED;
	}
	
	// edge detection
	// when moving right
	if (stage.map[rmx][my+1] == 0 && self->dx > 0)
	{
		self->dx = -ENEMY_MOVE_SPEED;
	}
	// when moving left
	if (stage.map[lmx][my+1] == 0 && self->dx < 0)
	{
		self->dx = +ENEMY_MOVE_SPEED;
	}
}



static void touch(Entity* other)
{
	// kill player if touching
	if (other == player)
	{
		player->health = 0;
	}
}
